
import pygame
import sys
import traceback
from pygame.locals import *

pygame.init()


# Couleur
background = (201, 202, 187)
checkerboard = (80, 80, 80)
button = (52, 53, 44)


# Dessiner l'échiquier
def Drawchessboard(screen):
    # Remplir la couleur d'arrière-plan
    screen.fill(background)
    Background = pygame.image.load("background.jpg").convert_alpha()
    screen.blit(Background, (0, 0))
    # Dessiner l'échiquier
    for i in range(21):
        pygame.draw.line(screen, checkerboard, (40 * i + 3, 3), (40 * i + 3, 803))
        pygame.draw.line(screen, checkerboard, (3, 40 * i + 3), (803, 40 * i + 3))
        # Tracer la ligne latérale
        pygame.draw.line(screen, checkerboard, (3, 3), (803, 3), 5)
        pygame.draw.line(screen, checkerboard, (3, 3), (3, 803), 5)
        pygame.draw.line(screen, checkerboard, (803, 3), (803, 803), 5)
        pygame.draw.line(screen, checkerboard, (3, 803), (803, 803), 5)

        # Dessiner le point d'ancrage
        pygame.draw.circle(screen, checkerboard, (163, 163), 6)
        pygame.draw.circle(screen, checkerboard, (163, 643), 6)
        pygame.draw.circle(screen, checkerboard, (643, 163), 6)
        pygame.draw.circle(screen, checkerboard, (643, 643), 6)
        pygame.draw.circle(screen, checkerboard, (403, 403), 6)

        # Dessiner les boutons «redémarrer» et «quitter»
        pygame.draw.rect(screen, button, [900, 500, 255, 100], 3)
        pygame.draw.rect(screen, button, [900, 650, 285, 100], 3)
        s_font = pygame.font.Font('font.ttf', 40)
        text2 = s_font.render("Redémarrer", True, button)
        text3 = s_font.render("Quitter le jeu", True, button)
        screen.blit(text2, (920, 520))
        screen.blit(text3, (920, 670))


# Dessiner des pièces d'échecs (abscisse, ordonnée, écran, couleur des pièces d'échecs (1 représente le noir, 2 représente le blanc))
def Drawchessman(x, y, screen, color):
    if color == 1:
        Black_chess = pygame.image.load("Black_chess.png").convert_alpha()
        screen.blit(Black_chess, (40 * x + 3 - 15, 40 * y + 3 - 15))
    if color == 2:
        White_chess = pygame.image.load("White_chess.png").convert_alpha()
        screen.blit(White_chess, (40 * x + 3 - 15, 40 * y + 3 - 15))


# Dessiner un échiquier avec des pièces
def Drawchessboardwithchessman(map, screen):
    screen.fill(background)
    Drawchessboard(screen)
    for i in range(24):
        for j in range(24):
            Drawchessman(i + 1, j + 1, screen, map[i][j])


# Définir la liste des échiquiers stockés,
# La liste est de 24 colonnes et 24 lignes car l'index dans la fonction de victoire dépassera 19

map = []
for i in range(24):
    map.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])


# Effacer la liste des MAP
def clear():
    global map
    for i in range(24):
        for j in range(24):
            map[i][j] = 0


# Déterminer si vous gagnez
def win(i, j):
    E = map[i][j]
    p = []
    for a in range(20):
        p.append(0)
    for i0 in range(i - 4, i + 5):
        for j0 in range(j - 4, j + 5):
            if (map[i0][j0] == E and i0 - i == j0 - j and i0 <= i and j0 <= j):
                p[0] += 1
            if (map[i0][j0] == E and j0 == j and i0 <= i and j0 <= j):
                p[1] += 1
            if (map[i0][j0] == E and i0 == i and i0 <= i and j0 <= j):
                p[2] += 1
            if (map[i0][j0] == E and i0 - i == j0 - j and i0 >= i and j0 >= j):
                p[3] += 1
            if (map[i0][j0] == E and j0 == j and i0 >= i and j0 >= j):
                p[4] += 1
            if (map[i0][j0] == E and i0 == i and i0 >= i and j0 >= j):
                p[5] += 1
            if (map[i0][j0] == E and i - i0 == j0 - j and i0 <= i and j0 >= j):
                p[6] += 1
            if (map[i0][j0] == E and i0 - i == j - j0 and i0 >= i and j0 <= j):
                p[7] += 1

    for b in range(20):
        if p[b] == 5:
            return True
    return False


# dessin le Rappel (capacité de type, écran, taille des mots)
def text(s, screen, x):
    # Couvrer d'abord la capacité de la dernière classe avec un rectangle
    pygame.draw.rect(screen, background, [850, 100, 1200, 100])
    # Définir la police et la taille
    s_font = pygame.font.Font('font.ttf', x)
    # Définir le contenu de la classe, que ce soit l'anti-aliasing, la couleur
    s_text = s_font.render(s, True, button)
    # Mettre le mot dans la position spécifiée de la fenêtre
    screen.blit(s_text, (880, 100))
    pygame.display.flip()


# Utilisé pour contrôler la séquence
t = True

# Utilisé pour arrêter le jeu après avoir terminé le jeu
running = True


# Fonction principale
def main():
    # Définir t, map, running pour être modifiable
    global t, map, running, maps, r, h

    # Zéro la MAP
    clear()

    # # Définisser une liste qui stocke tous les états de la carte
    # map2 = copy.deepcopy(map)
    # maps = [map2]

    # Définir la fenêtre
    screen = pygame.display.set_mode([1200, 806])

    # Définir le nom de la fenêtre
    pygame.display.set_caption("Gomoku")

    # Dessiner le tableau, le rappel et les boutons dans la fenêtre
    Drawchessboard(screen)
    pygame.display.flip()


    while True:
        # Seul running est vrai pour jouer aux échecs, il est principalement utilisé pour empêcher un autre mouvement après la fin de la partie
        if running:
            if t:
                color = 1
                text('Mouvement noir', screen, 30)
            else:
                color = 2
                text('Mouvement blanc', screen, 30)
        for event in pygame.event.get():
            # Cliquez sur 'x' pour fermer la fenêtre
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            # Cliquer sur la catégorie dans la fenêtre pour compléter l'instruction correspondante
            elif event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    x, y = event.pos[0], event.pos[1]
                    for i in range(19):
                        for j in range(19):
                            # Cliquer sur la position correspondante du plateau
                            if i * 40 + 3 + 20 < x < i * 40 + 3 + 60 and j * 40 + 3 + 20 < y < j * 40 + 3 + 60 and not map[i][j] and running:
                                # Placer les pièces de couleur correspondantes sur la position correspondante sur l'échiquier
                                Drawchessman(i + 1, j + 1, screen, color)
                                # Enregistrer la position du placement sur MAP
                                map[i][j] = color


                                # Déterminer s'il y a une ligne à cinq points après le placement
                                if win(i, j):
                                    if t:
                                        text('Noir gagne!', screen, 30)
                                    else:
                                        text('Blanc gagne!', screen, 30)
                                    # Empêcher un autre mouvement sur le tableau
                                    running = False
                    pygame.display.flip()
                    t = not t

                    # Si vous cliquez sur "Redémarrer"
                    if 900 < x < 1100 and 500 < y < 600:
                        # Débloquer
                        running = True
                        # Redémarrer
                        main()

                    # Cliquez sur "Quitter le jeu" pour quitter le jeu
                    elif 900 < x < 1100 and 650 < y < 750:
                        pygame.quit()
                        sys.exit()




if __name__ == "__main__":
    try:
        main()
    except SystemExit:
        pass
    except:
        traceback.print_exc()
    pygame.quit()

